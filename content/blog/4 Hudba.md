+++
title = "Hudba - Viktor Vlk"
+++

Hudba je mým bytostným projevem. 
Blízké mi jsou především bubny, rytmické a esenční nástroje.
V poslední době se rád mazlím i s kytarou, která hudbě, která skrze mě proudí dává další rozměr.

Některé písně jsou nahrány zde:
    {url = "https://soundcloud.com/viktorvlk", text = "🌩 Vlkův soundcloud"},

Další na možnost nahrání čekají a zatím znějí pouze naživo..